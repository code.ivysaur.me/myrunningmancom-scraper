
.PHONY: all
all: output/metadata.json

.PHONY: clean
clean:
	rm output/max_episode

output/max_episode:
	cd output && ../download-pages.sh

output/metadata.json: output/max_episode
	cd output && ../pages-to-json.php > metadata.json

